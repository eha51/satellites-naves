# Satellites - Nave

Programa para calcular la posición de una nave y su mensaje encriptado mediante tres satélites

## Explicación del problema

Se desea calcular la distancia a la que se encuentra la nave teniendo los siguientes datos:

Distancias de la nave a los satélites kenobi, skywalker y sato.
Coordenadas dimensionales de los satélites kenobi, skywalker y sato

![Alt text](assets/figura_1.png?raw=true "Figura")

Además en cada satélite se recibe un arreglo con palabras de un mensaje con un desfase y se desea determinar el mensaje  

## Pre-Requisitos

Se debe tener instalado los siguientes paquetes

```bash
npm >= v6.14.8
node >= v12.19.0
git >= v2.23.0
```

## Instalación 

Para instalar el proyecto necesita ejecutar los siguientes comandos.

```bash
git clone https://gitlab.com/eha51/satellites-naves.git
```
Y después 

```bash
npm i
```

## Ejecutar el aplicativo 

Para poder lanzar el aplicativo se debe ejecutar con el siguiente comando

> Tener en cuenta que por default se ejecuta en el puerto 3002 si desea cambiar el puerto necesita modificar la variable PORT en el  archivo .env


```bash
npm run start
```

## Solución del problema

Para la solución del problema se aplicó el algoritmo trilateración 2d.

Se basa en la ecuación de la circunferencia de tres círculos que tienen un punto tangencial en común donde intersectan, se toman las distancias hacia el punto de origen(En este caso sería la nave)  como los radios de los círculos y las coordenadas dimensionales con los centros de los círculos. Como se muestra en la siguiente figura:

![Alt text](assets/figura_2.png?raw=true "Figura")

Así al resolverse las ecuaciones se obtiene las coordenadas del punto tangencial en común donde intersectan los tres círculos(En este caso las coordenadas dimensionales de las naves).

> Nota: Existe un caso en el cual no se puede aplicar el algoritmo, se debe cumplir que la distancia entre los centros de los círculos sea menor o igual a la suma de los radios de ambos. Para no tener un caso de no intersección como el que se muestra en la figura:

![Alt text](assets/figura_3.png?raw=true "Figura")

## Proyecto en ejecución

Puedes realizar las pruebas del proyecto con POSTMAN en esta liga

```javascript
https://satellites-naves.herokuapp.com/
```

## Uso

### Endpoint POST /topsecret

Este método requiere las distancia del satélite a la nave y el arreglo de palabras del mensaje emitido por el mismo 

Payload
```javascript
{
    "satellites": [
        {
            "name": "kenobi",
            "distance": 500,
            "message": ["", "este", "es", "un", "mensaje"]
        },
        {
            "name": "skywalker",
            "distance": 424,
            "message": ["este", "", "un", "mensaje"]
        },
        {
            "name": "sato",
            "distance": 707,
            "message": ["", "", "es", "", "mensaje"]
        }
    ]
}
```

Respuesta en estatus 200

```javascript
{
    "position": {
        "x": -199.7,
        "y": 199.2
    },
    "message": "este es un mensaje"
}
```

### Endpoint POST /topsecret_split/:nombre_del_satelite

Este método requiere como parámetro el nombre del satélite(kenobi, skywalker, sato) y como payload la distancia hacia la nave y arreglo de palabras del mensaje para insertar la información en base de datos 

Payload
```javascript
{
    "distance": 707,
    "message": ["", "", "es", "", "mensaje"]
}
```

Respuesta en estatus 200

```javascript
{}
```

### Endpoint GET /topsecret_split

Este método obtiene la posición de la nave y el mensaje
 
>Nota: se tuvo que haber insertado los datos de los tres satélites con el endpoint anterior para que pueda realizar el cálculo, si no es así mandará un mensaje de error en un estatus 404

Respuesta en estatus 200

```javascript
{
    "position": {
        "x": -199.7,
        "y": 199.2
    },
    "message": "este es un mensaje"
}
```

### Casos del 404

En caso del error el endpoint devolverá en el siguiente formato la descripción del error que se ha propiciado

```javascript
{
    "details": # descripción del error
}
```

## Pruebas unitarias 

Puede ejecutar las pruebas del proyecto con el siguiente comando 
```bash
npm test
```


## Autor

Eduardo Hernández Angeles <eha.developer@gmail.com>
