/**
 * Error messages
 */
module.exports = {
  satellitesMissing: 'satellites are missing',
  notIntersectionPoint: 'there is no intersection with the point',
  requireBodyTopsecret: {
    msg: 'Invalid value',
    description: 'array of a maximum and minimum size of 3 with the name satellites' 
  },
  requireBodyTopsecretSplit: {
    msg: 'Invalid value',
    description: 'distance field is numeric and message field is array'
  },
  noFoundSatellite: 'satellite name not found',
  notExistInfo: 'there is no information from the three satellites to calculate the position',
  notFoundMessage: 'error getting message'
}