/**
 * Names of proposed satellites
 */
module.exports = {
  kenobi: 'kenobi',
  skywalker: 'skywalker',
  sato: 'sato'
}