/**
 * Management of temporary information storage
 */
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

//File location where you store the info
const adapter = new FileSync('db/db_satellites.json')
const db = low(adapter)
 
// Set some defaults
db.defaults({
  satellites: {
    kenobi: null,
    skywalker: null,
    sato: null
  }
}).write()

// Export db model methods
module.exports = db;
 