/**
 * Modules to start the server
 * 
 * express: plugin for server infrastructure
 * body-parser: Parse incoming request bodies in a middleware before your handlers, available under the req.body property.
 * helmet: helps you secure your Express apps by setting various HTTP headers
 * coors:  helps to configure the types of requests
 * dotenv: is a zero dependency module that loads environment variables from an .env
 */
const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var cors = require('cors')
const app = express();

require('dotenv').config()

/**
 * Middlewares configuration for project initialization
 */
app.use(cors())
app.use(bodyParser.json());
app.use(express.urlencoded({extended: false}));
app.use(helmet());

//Set endpoints of topscrect
app.use('/',require('./routes/topSecret'))

// Endpoint to check the status of the server
app.get('/check',(req,res) => {
  res.status(200).send({});
})

// Set the server port
app.set('port',process.env.PORT || 3002);

// Server initialization
var server  = app.listen(app.get('port'),() => {
  console.log('server on port ',app.get('port'));
})

module.exports = server;