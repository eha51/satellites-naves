// Method to validate the middleware of each request
const { validationResult} = require('express-validator');
// Export of models module
const model = require('../models/topSecretModel');
// Names of the satellites
const typeSatellites = require('../constants/typeSatellitesConstant');
// Methods retrieve location of point of origin
const { getLocation,getLocationForSatellite } = require('../utils/getLocation');
// Menssages Erro
const { 
  satellitesMissing,
  noFoundSatellite,
  notExistInfo,
  requireBodyTopsecret,
  requireBodyTopsecretSplit
} = require('../constants/messageErrorConstants');
// Methods to recover the encrypted message
const getMessage = require('../utils/getMessage');
/**
 * Generic method to return the error
 * 
 * @param {string} error message error
 * 
 * @return error in object
 * 
 */
const errorDetail = (error) => {
  return {'details': error}
};
/**
 * mMthod to verify the existence of a key in an object
 * 
 * @param {object} obj object 
 * @param {key} key key to find
 * 
 * @returns {boolean} if the key exists in the object
 */
const isKeyExists = (obj,key) => {
  return key in obj;
}

module.exports = {
  /**
   * Method POST / topecret endpoint to get the position of the sender and his message
   * 
   * @param req request 
   * @param res response
   */
  topsecret: (req,res) => {
    try {
      const result = validationResult(req);
      if(!result.isEmpty()) throw requireBodyTopsecret;
      const satellites = req.body.satellites;
      let distanceOfSatellites = {
        kenobi: null,
        skywalker: null,
        sato: null
      }
      let messagesOfSatellites = {
        kenobi: null,
        skywalker: null,
        sato: null
      }
      satellites.forEach((satellite) => {
        if(isKeyExists(distanceOfSatellites,satellite.name)) {
          distanceOfSatellites[satellite.name] = satellite.distance;
          messagesOfSatellites[satellite.name] = satellite.message;
        }
        else {
          throw satellitesMissing;
        }
      })
      const pos = getLocation(
        distanceOfSatellites.kenobi,
        distanceOfSatellites.skywalker,
        distanceOfSatellites.sato
      );
      const message = getMessage(
        messagesOfSatellites.kenobi,
        messagesOfSatellites.skywalker,
        messagesOfSatellites.sato
      );
      res.status(200).send({
        position: {
          ...pos
        },
        message
      });
    } catch (error) {
      res.status(404).send(errorDetail(error));
    }

  },
   /**
   *  Method POST /topecret_split/:name_satellite endpoint to get insert the distance from the satellite to the sender and his message
   * 
   * @param req request 
   * @param res response
   */
  topsecretSplitSatellite: async(req,res) => {
    try {
      const result = validationResult(req);
      if(!result.isEmpty()) throw requireBodyTopsecretSplit;
      const { distance,message } = req.body;
      switch(req.params.satellite) {
        case typeSatellites.kenobi:
        case typeSatellites.skywalker:
        case typeSatellites.sato:
          await model.set(
            `satellites.${req.params.satellite}`,
            {
              distance,
              message: message
            }
          ).write();
          break;
        default:
          throw noFoundSatellite
      }
      res.status(200).send({})
    } catch (error) {
      res.status(404).send(errorDetail(error));
    }

  },
   /**
   *  Method GET /topecret_split endpoint to get calculates the position and message of the source, if you have the data from three satellites
   * 
   * @param req request 
   * @param res response
   */
  topsecretSplit: (req,res) => {
    try {
      const result = validationResult(req);
      if(!result.isEmpty()) throw requireBodyTopsecretSplit;
      const satellitesDB = model.get('satellites').value();
      let satellites = [];
      let messagesOfSatellites = [];
      for (const key in satellitesDB) {
        if(satellites[key] === null) {
          throw notExistInfo;
        }
        else {
          satellites.push({
            name: key,
            ...satellitesDB[key]
          })
          messagesOfSatellites.push(satellitesDB[key]['message'])
        }
      }
      const pos = getLocationForSatellite(satellites);
      const message = getMessage(
        messagesOfSatellites[0],
        messagesOfSatellites[1],
        messagesOfSatellites[2]
      );
      res.status(200).send({
        position: {
          ...pos
        },
        message
      })
    } catch (error) {
      res.status(404).send(errorDetail(error));
    }

  }
}