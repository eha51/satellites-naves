// Message error
const { notFoundMessage } = require('../constants/messageErrorConstants')

/**
 * Methods that validate a word of the message and that is 
 * not repeated in the words already stored in the message
 * 
 * @param {number} pos counter of array
 * @param {string} messageReceive message word
 * @param {Array} messageStorage array of non-repeating message words
 * 
 * @returns {boolean} if the word is valid
 */
var validationMessage = 
  (pos,messageReceive,messageStorage) => (
    pos < messageReceive.length && 
    messageReceive[pos] !== null &&
    typeof messageReceive[pos] === 'string' &&
    messageReceive[pos].length > 0 &&
    !messageStorage.includes(messageReceive[pos])
  )

/**
 * Recursive methods that get the outdated message from three arrays
 * 
 * @param {Array} messageA first satellite message
 * @param {Array} messageB second satellite message
 * @param {Array} messageC third satellite message
 * @param {Array} message message to get
 * @param {number} pos count of array
 * @param {number} size size of count
 * 
 * @returns {string} decrypted message
 */
function getMessage(messageA,messageB,messageC,message,pos,size){
  try {
    if(size === pos) return message.join(' ');

    if(validationMessage(pos,messageA,message)) message.push(messageA[pos]);

    if(validationMessage(pos,messageB,message)) message.push(messageB[pos]);

    if(validationMessage(pos,messageB,message)) message.push(messageC[pos]);

    pos++;

    return getMessage(messageA,messageB,messageC,message,pos,size);
  } catch (error) {
    throw 'error get message';
  }
}
/**
 * Methods that determine the message based on three word arrays from the satellites
 * 
 * @param {Array} messageA first satellite message
 * @param {Array} messageB second satellite message
 * @param {Array} messageC third satellite message
 * 
 * @example entry messages
 * 
 * (
 *  ["", "este", "es", "un", "mensaje"],
 *  ["este", "", "un", "mensaje"],
 *  ["", "", "es", "", "mensaje"]
 * )
 * 
 * @returns {string} decrypted message
 * 
 * @example reponse 
 * 
 * este es un menseje
 */
module.exports = (messageA,messageB,messageC) => {
  try {
    const sizeMax = Math.max(messageA.length,messageB.length,messageC.length);
    if(sizeMax === 0) throw notFoundMessage;
    const pos = 0;
    const message = getMessage(messageA,messageB,messageC,[],pos,sizeMax);

    return message;
  } catch (error) {
    throw error;
  }

}