//mathematical operations
const operations = require('./operations');
/**
 * Trilateration algorithm to obtain the position of the source 
 * from the distances to the points and their coordinates
 */
const trilateration = require('node-trilateration');
// Message error
const { notIntersectionPoint } = require('../constants/messageErrorConstants');

/**
 * Method that returns the dimensional position of the validation 
 * source that can be applied to the Trilateration algorithm
 * 
 * @param {Object} satelliteA coordinates of satellite A and distance to the source
 * @param {Object} satelliteB coordinates of satellite B and distance to the source
 * @param {Object} satelliteC coordinates of satellite C and distance to the source
 * 
 * @example satellite object
 * {
 *  x:100.0,
 *  y: -200.0,
 *  distance: 100
 * }
 * 
 * @returns source coordinates
 * 
 * @example 
 * {
 *  x:100.0,
 *  y:20.0
 * }
 */
module.exports = (
      satelliteA,
      satelliteB,
      satelliteC
    ) => {
    try {
      const { x:x1,y:y1,distance:distanceToPointA } = satelliteA;
      const { x:x2,y:y2,distance:distanceToPointB } = satelliteB;
      const { x:x3,y:y3,distance:distanceToPointC } = satelliteC;
      const distanceBetweenAAndB = operations.distanceBetweenTwoPoints(x1,y1,x2,y2);
      const distanceBetweenAAndC = operations.distanceBetweenTwoPoints(x1,y1,x3,y3);
      const distanceBetweenBAndC = operations.distanceBetweenTwoPoints(x2,y2,x3,y3);

      if(
        operations.isValidIntersection(distanceToPointA,distanceToPointB,distanceBetweenAAndB) &&
        operations.isValidIntersection(distanceToPointA,distanceToPointC,distanceBetweenAAndC) &&
        operations.isValidIntersection(distanceToPointB,distanceToPointC,distanceBetweenBAndC)
      ) {
        const beacons = [
          {x: x1, y: y1, distance: distanceToPointA},
          {x: x2, y: y2, distance: distanceToPointB},
          {x: x3, y: y3, distance: distanceToPointC}
        ];


        const pos = trilateration.calculate(beacons);

        var roundNumber = (numb) => Math.round((numb + Number.EPSILON) * 10) / 10;

        return {
          x: roundNumber(pos.x),
          y: roundNumber(pos.y)
        }
      }
      else {
        throw notIntersectionPoint
      }
    } catch (error) {
        throw error;
    }
}