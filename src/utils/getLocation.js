// satellite names
const typeSatellites = require('../constants/typeSatellitesConstant');
/**
 * Trilateration algorithm to calculate the position of 
 * the source based on three points and their respective distances
 */
const mathTrilateration = require('../utils/mathTrilateration');
// satellite position
const satellitesPos = require('../constants/satellitesConstants');
// Menssages error
const { satellitesMissing } = require('../constants/messageErrorConstants');
/**
 * Trilateration algorithm to calculate the position of 
 * the source based on three points and their respective distances
 */
module.exports = {
  /**
   * Method that calculates the position of the source
   * 
   * @param {number} distanceToKenobi distance from source to kenobi
   * @param { number} distanceToSkywalker distance from source to skywalker
   * @param { number} distanceToSato distance from source to Sato
   * 
   * @returns {Object} source position
   * 
   * @example return method
   * {
   *  x: 24.0,
   *  y: 10.2
   * }
   */
  getLocation: (distanceToKenobi,distanceToSkywalker,distanceToSato) => {
    try {
      return mathTrilateration(
        {
          distance: distanceToKenobi,
          ...satellitesPos.kenobi
        },
        {
          distance: distanceToSkywalker,
          ...satellitesPos.skywalker
        },
        {
          distance: distanceToSato,
          ...satellitesPos.sato
        }
      )
    } catch (error) {
      throw error;
    }
  },
  /**
   * Method that calculates the position of the source
   * 
   * @param {Array} satellites satellite array with their parameters
   * 
   * @example satellites object
   * [
   *  {
   *    name: 'kenobi',
   *    distance: 100.0,
   *    message: ["", "este", "es", "un", "mensaje"]
   *  }
   * ]
   * 
   * @returns {Object} source position
   * 
   * @example return method
   * 
   * {
   *  x: 24.0,
   *  y: 10.2
   * }
   */
  getLocationForSatellite: (satellites) => {
    try {
      let namesSatellites = [];
      for (const key in typeSatellites) {
        namesSatellites.push(key);
      }
      let satellitesWithDistanceAndPos = [];
      satellites.forEach(satellite => {
        if(namesSatellites.includes(satellite.name)) {
          satellitesWithDistanceAndPos.push({
            ...satellite,
            ...satellitesPos[satellite.name]
          })
        }
      });
      if(satellitesWithDistanceAndPos.length !== 3) {
        throw satellitesMissing
      }
      return mathTrilateration(
        satellitesWithDistanceAndPos[0],
        satellitesWithDistanceAndPos[1],
        satellitesWithDistanceAndPos[2]
      )
    } catch (error) {
      throw error;
    }
  }
}
