
module.exports = {
  /**
   * Method that calculates the distance between two points
   * 
   * @param {number} x1 abscissa of point A
   * @param {number} y1 ordered of point A
   * @param {number} x2 abscissa of point B
   * @param {number} y2 ordered of point B
   * 
   * @returns {number} distance between two points
   */
  distanceBetweenTwoPoints: (x1,y1,x2,y2) => {
    try {
      return Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2))
    } catch (error) {
      throw error;
    }
  },
  /**
   * Method that calculates if there is an intersession between two circles
   * 
   * @param {number}  r1 radius of circle A
   * @param {number}  r1 radius of circle A
   * 
   * @returns { boolean } there is or there is no intersession between the circuits
   */
  isValidIntersection: (r1,r2,d) => {
    try {
      return !(d>r1+r2)
    } catch (error) {
      throw error;
    }
  }
}