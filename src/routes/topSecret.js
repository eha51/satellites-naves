/**
 * Routes handling configuration with express
 */
const express = require('express');
const router = express.Router();
const validators = require('../middlewares/validators');
const ctrl = require('../controllers/topSecretCtrl');

// Server paths
router.post('/topsecret',validators.topsecrect,ctrl.topsecret);
router.post('/topsecret_split/:satellite',validators.topsecrectSplitSatellite,ctrl.topsecretSplitSatellite);
router.get('/topsecret_split',ctrl.topsecretSplit);

module.exports = router;
