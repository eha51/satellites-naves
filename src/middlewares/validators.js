// Methods to validate the request parameters
const { body,check} = require('express-validator');

module.exports = {
  /**
   * Middleware POST /topsecret endpoint parameter validation
   * 
   * @param  {Array} satellites array of size 3 objects
   * @param {number} distance is numeric
   * @param {string} name is string
   * @param {string} message is string
   */
  topsecrect: [
    body('satellites').exists().isArray({min:3,max:3}),
    check('satellites.*.distance').exists().isNumeric(),
    check('satellites.*.name').exists().isString(),
    check('satellites.*.message').exists().isArray()
  ],
  /**
   * Middleware POST /topsecret_split/:name_satellite endpoint parameter validation
   * 
   * @param {number} distance is numeric
   * @param {string} message is string
   */
  topsecrectSplitSatellite: [
    body('distance').exists().isNumeric(),
    body('message').exists().isArray()
  ]
}