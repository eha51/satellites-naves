const {
  requireBodyTopsecret,
  notIntersectionPoint,
  satellitesMissing,
  noFoundSatellite,
  requireBodyTopsecretSplit
} = require('../../src/constants/messageErrorConstants');

const satellitesBody = {
  "satellites": [
      {
          "name": "kenobi",
          "distance": 500,
          "message": ["", "este", "es", "un", "mensaje"]
      },
      {
          "name": "skywalker",
          "distance": 424,
          "message": ["este", "", "un", "mensaje"]
      },
      {
          "name": "sato",
          "distance": 707,
          "message": ["", "", "es", "", "mensaje"]
      }
  ]
};

const satellitesBodyNotIntersecion = {
  "satellites": [
      {
          "name": "kenobi",
          "distance": 1,
          "message": []
      },
      {
          "name": "skywalker",
          "distance": 1,
          "message": []
      },
      {
          "name": "sato",
          "distance": 1,
          "message": []
      }
  ]
};
const satellitesBodyAreMissing = {
  "satellites": [
      {
          "name": "kenobix",
          "distance": 1,
          "message": []
      },
      {
          "name": "skywalker",
          "distance": 1,
          "message": []
      },
      {
          "name": "sato",
          "distance": 1,
          "message": []
      }
  ]
};

const bodySatelliteName = {
  "distance": 707,
  "message": []
};

module.exports = {
  topsecretPost: {
    body: {
      ...satellitesBody
    },
    responseStatus200: {
        "position": {
            "x": -199.7,
            "y": 199.2
        },
        "message": "este es un mensaje"
    },
    requestBodyStatus404: [
      {
        'satellites3': {...satellitesBody.satellites}
      },
      {
        ...satellitesBodyNotIntersecion
      },
      {
        ...satellitesBodyAreMissing
      }
    ],
    reponseStatus404: [
      requireBodyTopsecret,
      notIntersectionPoint,
      satellitesMissing
    ]
  },
  topsecretPostSplitName: {
    body: {
      "distance": 707,
      "message": []
    },
    responseStatus200: {},
    nameSatellites: [
      'kenobi',
      'skywalker',
      'sato'
    ],
    requestBodyStatus404: [
      {
        "distancexx": 707,
        "message": 23
      },
      {
        ...bodySatelliteName
      },
    ],
    reponseStatus404: [
      requireBodyTopsecretSplit,
      noFoundSatellite,
    ]
  },
  topsecretPostSplit: [
    {
        "distance": 500,
        "message": ["", "este", "es", "un", "mensaje"]
    },
    {
        "distance": 424,
        "message": ["este", "", "un", "mensaje"]
    },
    {
        "distance": 707,
        "message": ["", "", "es", "", "mensaje"]
    }
  ]
}