var Request = require('request');
const reqAndRes = require('./constants/requestAndResponse');
const port = process.env.PORT || 3002;

describe('Server',() => {
  var server;
  var URL_BASE = `http://localhost:${port}`;
  var body = reqAndRes.topsecretPost.body;
  var failsBody = [...reqAndRes.topsecretPost.requestBodyStatus404];
  var failsResponse = [...reqAndRes.topsecretPost.reponseStatus404];
  var i = 0;
  var bodySplit = reqAndRes.topsecretPostSplitName.body;
  var failsBodySplit = [...reqAndRes.topsecretPostSplitName.requestBodyStatus404];
  var failsResponseSplit = [...reqAndRes.topsecretPostSplitName.reponseStatus404];
  var nameSatellites = [
    ...reqAndRes.topsecretPostSplitName.nameSatellites
  ];
  var requestInsertSatellite = (url,body) => {
    return new Promise((resolve) => {
      Request.post({
        url,
        body,
        json: true
      },(error,response,bodyResponse) => {
        resolve();
      })
    })
  }
  var nameSatellitsFails = [
    'kenobi',
    'xxx'
  ];
  var j = 0;
  var x = 0;
  var y = 0;

  beforeAll(() => {
    server = require('../src/server');
  })
  afterAll(() => {
    server.close();
  })

  describe('Endpoint /topsecret POST successful',() => {
    var data = {};
    beforeEach((done) => {
      Request.post({
        url: `${URL_BASE}/topsecret`,
        body: body,
        json: true
      },(error,response,bodyResponse) => {
        data.status = response.statusCode;
        data.body = bodyResponse;
        done();
      })
    });

    it('should response code 200',() => {
      expect(data.status).toEqual(200);
      expect(data.body).toEqual(reqAndRes.topsecretPost.responseStatus200);
    })
  })

  describe('Endpoint /topsecret POST fails',() => {
    var data = {};
    
    beforeAll(() => {
      i = 0;
    })

    beforeEach((done) => {
      Request.post({
        url: `${URL_BASE}/topsecret`,
        body: failsBody[i],
        json: true
      },(error,response,bodyResponse) => {
        data.status = response.statusCode;
        data.body = bodyResponse;
        done();
      })
    });

    afterEach(() => {
      i++;
    })

    it('should response code 404 invalid params of payload',() => {
      expect(data.status).toEqual(404);
      expect(data.body).toEqual({
        details:failsResponse[i]
      });
    })

    it('should response code 404 there is no intersection with the point',() => {
      expect(data.status).toEqual(404);
      expect(data.body).toEqual({
        details: failsResponse[i]
      });
    })

    it('should response code 404 satellites are missing',() => {
      expect(data.status).toEqual(404);
      expect(data.body).toEqual({
        details: failsResponse[i]
      });
    })
  })

  describe('Endpoint /topsecret_split/:name_satellite successful',() => {
    var data = {};
    beforeEach((done) => {
      Request.post({
        url: `${URL_BASE}/topsecret_split/${nameSatellites[j]}`,
        body: bodySplit,
        json: true
      },(error,response,bodyResponse) => {
        data.status = response.statusCode;
        data.body = bodyResponse;
        done();
      })
    });

    afterEach(() => {
      j++
    })

    it('should response code 200 satellite kenobi',() => {
      expect(data.status).toEqual(200);
      expect(data.body).toEqual(reqAndRes.topsecretPostSplitName.responseStatus200);
    })

    it('should response code 200 satellite skywolker',() => {
      expect(data.status).toEqual(200);
      expect(data.body).toEqual(reqAndRes.topsecretPostSplitName.responseStatus200);
    })

    it('should response code 200 satellite sato',() => {
      expect(data.status).toEqual(200);
      expect(data.body).toEqual(reqAndRes.topsecretPostSplitName.responseStatus200);
    })
  })

  describe('Endpoint /topsecret_split/:name_satellite POST fails',() => {
    var data = {};

    beforeEach((done) => {
      Request.post({
        url: `${URL_BASE}/topsecret_split/${nameSatellitsFails[y]}`,
        body: failsBodySplit[x],
        json: true
      },(error,response,bodyResponse) => {
        data.status = response.statusCode;
        data.body = bodyResponse;
        done();
      })
    });

    afterEach(() => {
      x++;
      y++;
    })

    it('should response code 404 invalid params of payload',() => {
      expect(data.status).toEqual(404);
      expect(data.body).toEqual({
        details: failsResponseSplit[x]
      });
    })

    it('should response code 404 satellite name not found',() => {
      expect(data.status).toEqual(404);
      expect(data.body).toEqual({
        details: failsResponseSplit[x]
      });
    })

  })

  describe('Endpoint /topsecret_split/ GET',() => {
    var data = {};

    it('should response code 200 satellite kenobi',async() => {
      await requestInsertSatellite(`${URL_BASE}/topsecret_split/${nameSatellites[0]}`,reqAndRes.topsecretPostSplit[0]);
      await requestInsertSatellite(`${URL_BASE}/topsecret_split/${nameSatellites[1]}`,reqAndRes.topsecretPostSplit[1]);
      await requestInsertSatellite(`${URL_BASE}/topsecret_split/${nameSatellites[2]}`,reqAndRes.topsecretPostSplit[2]);
      Request.get(`${URL_BASE}/topsecret_split`,
      (error,response,bodyResponse) => {
        data.status = response.statusCode;
        data.body = bodyResponse;
        expect(data.status).toEqual(200);
        expect(JSON.parse(data.body)).toEqual(reqAndRes.topsecretPost.responseStatus200);
      })
    })
  })

})